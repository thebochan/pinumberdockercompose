import application.Application;
import application.model.PiResults;
import application.service.PiHandler;
import application.service.PiService;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Application.class)

public class RestDockerTest {

   @ClassRule
    public static GenericContainer container1 = new FixedHostPortGenericContainer("dockercompose_calculate:latest").withFixedExposedPort(9090, 8080)
           .withFixedExposedPort(9091, 8080).withFixedExposedPort(9092, 8080);
    @ClassRule
    public static GenericContainer container = new FixedHostPortGenericContainer("mongo").withFixedExposedPort(27017, 27017);


    @Autowired
    private PiHandler piHandler;

    @Autowired
    private PiService piService;

    @Test
    public void testContainer() {
        Mono<PiResults> response = piHandler.pi(1000L);
        Flux<PiResults> show = piService.get();
        Double expectedResult = 3.1425916543395442;
        Assert.assertNotNull(show);
        Assert.assertEquals("pi = " + expectedResult.toString(), response.block().getResult());
    }


}
