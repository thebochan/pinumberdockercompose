package application.service;

import application.model.PiResults;
import application.repository.PiRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@Service
public class PiService {

    private PiRepository piRepository;

    public Flux<PiResults> get(){
        return piRepository.findAll();
    }

    public Mono<PiResults> save(PiResults piResults){
        return piRepository.save(piResults);
    }
}
