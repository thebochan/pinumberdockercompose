package application.service;

import application.model.PiResults;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Component
public class PiHandler {

    public PiHandler(PiService piService){
        this.piService = piService;
    }

    private PiService piService;

    public Mono<PiResults> pi(Long n) {
        String pattern = "MMMMM dd yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        String url = "http://localhost:909" + new Random().nextInt(3) + "/pi?n=" + n;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return piService.save(new PiResults(date, result));
    }
}