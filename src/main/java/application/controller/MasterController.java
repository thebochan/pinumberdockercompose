package application.controller;

import application.model.PiResults;
import application.service.PiHandler;
import application.service.PiService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class MasterController {

    MasterController(PiService piService, PiHandler piHandler){
        this.piService = piService;
        this.piHandler = piHandler;
    }

    private PiService piService;
    private PiHandler piHandler;

    @GetMapping("/show")
    public Flux<PiResults> showResult(){
        return piService.get();
    }

    @GetMapping("/pi")
    public Mono<PiResults> calculatePi(@RequestParam Long n){
        return piHandler.pi(n);
    }
}