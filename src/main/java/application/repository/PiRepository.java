package application.repository;

import application.model.PiResults;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

public interface PiRepository extends ReactiveCrudRepository<PiResults, String> {

}
