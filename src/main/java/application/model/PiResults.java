package application.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "picollection")
public class PiResults {

    @Id
    private String id;
    private String date;
    private String result;

    @PersistenceConstructor
    public PiResults(String date, String result){
        this.date = date;
        this.result = result;
    }

}
